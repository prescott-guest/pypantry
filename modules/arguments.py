#!/usr/bin/env python3

# pypantry
# Copyright (C) 2018 Prescott Hidalgo-Monroy

# This file is part of pypantry.
#
# pypantry is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypantry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypantry.  If not, see <http://www.gnu.org/licenses/>.

import argparse

__git__ = 'https://salsa.debian.org/prescott-guest/pypantry'

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawTextHelpFormatter,
    description='CLI interface for the PyPantry recipe database program',
    epilog='Source code and developer info can be found here:'
    '\n{}'.format(__git__)
)
parser.add_argument('-l', '--load',
                    action='store_true',
                    help='Load CSV files into PyPantry'
                    )
parser.add_argument('-i', '--insert',
                    action='store_true',
                    help='Input a recipe into PyPantry.'
                    )
parser.add_argument('-r', '--random',
                    action='store_true',
                    help='Choose a random recipe from your cookbook'
                    )
parser.add_argument('-s', '--set',
                    nargs='+',
                    help='Set a configuration option from the command line.'
                    '\nThe default location for "config.ini" is '
                    '~/.config/pypantry/'
                    '\ne.g. pypantry -s database path ~/new/db/path.db'
                    )
parser.add_argument('-f', '--find',
                    nargs='+',
                    help='Find the right recipe using search words.'
                    '\nOptions can be chained together e.g. -f meal '
                    'breakfast serv 4'
                    '\n\nname [QUERY]   :   Searches by recipe name'
                    '\ndesc [QUERY]   :   Searches by description'
                    '\nmeal [QUERY]   :   Searches by meal type'
                    '\ncuis [QUERY]   :   Searches by cuisine'
                    '\nclas [QUERY]   :   Searches by class'
                    '\nserv [QUERY]   :   Searches by serving size'
                    '\ning  [QUERY]   :   Searches by ingredient'
                    )

args = parser.parse_args()
