#!/usr/bin/env python3

# pypantry
# Copyright (C) 2018 Prescott Hidalgo-Monroy

# This file is part of pypantry.
#
# pypantry is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypantry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypantry.  If not, see <http://www.gnu.org/licenses/>.

import random
import pandas as pd
from modules import init

pd.set_option('display.max_colwidth', -1)
pd.set_option('display.colheader_justify', 'center')

init = init.Init()


class Output:
    def find(self, query):
        '''Outputs relevant recipes following user search query.

        Leads into a separate pull function to bring up a fully detailed
        output of the chosen recipe.'''

        d = {
            'name': None,
            'desc': None,
            'meal': None,
            'cuis': None,
            'clas': None,
            'serv': None,
            'ing': None
        }
        temp = []

        # Appends all search queries into its correct key in the dictionary
        # and formats for SQL "LIKE"
        for i in query:
            if i in d:
                temp.append(i)
            else:
                if d[temp[-1]]:
                    d[temp[-1]] = d[temp[-1]]+' '+i+'%'
                else:
                    d[temp[-1]] = '%'+i+'%'
        for i in d:
            if d[i] is None:
                d[i] = '%'

        # Runs SQL queries based off of search terms
        dfr = pd.read_sql('''
                SELECT id,name,description,meal,
                cuisine,class,servings
                FROM Recipes WHERE
                name LIKE :name AND
                description LIKE :desc AND
                meal LIKE :meal AND
                cuisine LIKE :cuis AND
                class LIKE :clas AND
                servings LIKE :serv;
                ''', init.conn, params=d, index_col='id')

        dfi = pd.read_sql('''
                SELECT r.id, r.name, i.name
                FROM Ingredients as i
                INNER JOIN RecipeIngredients as ri
                ON i.id = ri.ingredient_id
                INNER JOIN Recipes as r
                ON ri.recipe_id = r.id
                WHERE i.name LIKE :ing AND
                r.name LIKE :name AND
                r.description LIKE :desc AND
                r.meal LIKE :meal AND
                r.cuisine LIKE :cuis AND
                r.class LIKE :clas AND
                r.servings LIKE :serv;
                ''', init.conn, params=d, index_col='id')

        # Formats and prints SQL results as DataFrames for easy user parsing
        if d['ing'] == '%':
            print(dfr)
        elif (d['name'] == '%' and d['desc'] == '%' and d['meal'] == '%'
                and d['cuis'] == '%' and d['clas'] == '%' and d['serv'] == '%'
                and d['ing'] != '%'):
            print(dfi)
        else:
            result = re.sub('%', '', d['ing'])
            print(dfr)
            print('\nYou also searched Ingredients for "{}"'
                  '\nBelow are those results:\n'.format(result))
            print(dfi)

        # Upon correct input, will show full details of chosen recipe
        # in pull()
        while True:
            print('\nPlease select a recipe above by inputting its'
                  '\nid number in order to view the full details, '
                  '\nor type "exit" to abort.\n')
            answer = input('Choose recipe: ')
            if answer[0].lower() == 'e':
                print('\nGoodbye!')
                break
            else:
                try:
                    int(answer)
                    self.pull(answer)
                    break
                except ValueError:
                    print('\nChoose a correct value and please try again.')

    def pull(self, answer):
        'Gets the detailed recipe information from the output of the search.'

        ls = []
        ls.append(answer)
        result = pd.read_sql('''
                SELECT DISTINCT r.name as name,
                r.description, r.meal, r.cuisine,
                r.class as clas,  r.servings,
                i.name as ingredient,
                ri.amount, ri.detail,
                rs.step_number, rs.instructions
                FROM Ingredients as i
                INNER JOIN RecipeIngredients as ri
                ON i.id = ri.ingredient_id
                INNER JOIN RecipeSteps as rs
                ON ri.recipe_id = rs.recipe_id
                INNER JOIN Recipes as r
                ON rs.recipe_id = r.id
                WHERE r.id = ?;
                ''', init.conn, params=ls)

        # Formats and prints recipe header information
        print('\n'+result.name[0]+'\n' +
              result.description[0])
        w = len(result.name[0])
        print('-'*w)
        print('Meal: '+result.meal[0]+'\n'
              'Cuisine: '+result.cuisine[0]+'\n'
              'Class: '+result.clas[0]+'\n'
              'Servings: '+result.servings[0]+'\n'
              )

        # Formats and prints recipe ingredients information
        ing_d = {'name': [], 'detail': [], 'amount': []}
        ing_results = list(
            zip(result.ingredient, result.detail, result.amount))
        ing_results = pd.Series(ing_results).unique()
        for i in ing_results:
            ing_d['name'].append(i[0])
            ing_d['detail'].append(i[1])
            ing_d['amount'].append(i[2])
        final = pd.DataFrame(data=ing_d, columns=['name', 'detail', 'amount'])
        print('Ingredient list')
        print('-'*w)
        print(final)

        # Formats and prints recipe steps information
        step_d = {'step_number': [], 'instructions': []}
        steps = list(zip(result.step_number, result.instructions))
        steps = pd.Series(steps).unique()
        for i in steps:
            step_d['step_number'].append(i[0])
            step_d['instructions'].append(i[1])
        step_final = pd.DataFrame(data=step_d, columns=[
                                  'step_number', 'instructions'])
        step_final.sort_values('step_number', ascending=True)
        print('\nRecipe steps')
        print('-'*w)
        print(step_final)

    def random(self):
        '''Randomly selects a recipe and pulls the detailed info.'''

        top = pd.read_sql('SELECT max(id) as id from Recipes;', init.conn)
        top = top.id[0]
        rand = random.randrange(1, top+1)
        self.pull(rand)
