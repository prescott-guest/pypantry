#!/usr/bin/env python3

# pypantry
# Copyright (C) 2018 Prescott Hidalgo-Monroy

# This file is part of pypantry.
#
# pypantry is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypantry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypantry.  If not, see <http://www.gnu.org/licenses/>.

import os
import configparser
from modules import init

i = init.Init()


def setting(query):
    '''CLI setting of available configuration options.'''

    if query[0] in i.config.sections():
        if query[1] in i.config[query[0]]:

            if query[1] == 'path' and os.path.exists(query[2]):
                i.config[query[0]][query[1]] = query[2]
                with open(i.config_path, 'w') as config_file:
                    i.config.write(config_file)
            elif query[1] == 'path' and not os.path.exists(query[2]):
                if query[0] == 'database':
                    if query[2][0] == '~':
                        path = i.homedir+query[2][1:]
                    else:
                        path = query[2]
                    di = os.path.dirname(path)
                    try:
                        os.makedirs(di)
                    except FileExistsError:
                        pass
                    os.rename(i.config['database']['path'], path)
                    i.config[query[0]][query[1]] = query[2]
                    with open(i.config_path, 'w') as config_file:
                        i.config.write(config_file)
                elif query[0] == 'import':
                    if query[2][0] == '~':
                        path = i.homedir+query[2][1:]
                    else:
                        path = query[2]
                    try:
                        os.makedirs(path)
                    except FileExistsError:
                        pass
                    try:
                        os.rmdir(i.config[query[0]][query[1]])
                    except OSError:
                        pass
                    i.config[query[0]][query[1]] = query[2]
                    with open(i.config_path, 'w') as config_file:
                        i.config.write(i.config_file)
            else:
                i.config[query[0]][query[1]] = query[2]
                with open(i.config_path, 'w') as config_file:
                    i.config.write(config_file)
        else:
            print('Please choose a correct i.config value and try again.\n'
                  '"{}" is not a i.configuration option for '
                  '"{}"'.format(query[1], query[0])
                  )
    else:
        print('Please choose a correct i.config value and try again.\n'
              '{} is not a i.configuration section'.format(query[0])
              )
