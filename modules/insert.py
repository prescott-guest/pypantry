#!/usr/bin/env python3

# pypantry
# Copyright (C) 2018 Prescott Hidalgo-Monroy

# This file is part of pypantry.
#
# pypantry is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypantry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypantry.  If not, see <http://www.gnu.org/licenses/>.

import pandas as pd
from modules import init

init = init.Init()


class Insert:
    def __init__(self):
        self.intro = (
            'Welcome to the interactive mode of PyPantry!'
            '\n\nFollow the instructions in order to insert new recipe.\n'
            'You will have a chance to review before submitting.\n'
        )
        self.correct = (
            'Is the following correct? [y/n]: '
        )
        self.try_again = ('Please choose a correct value and try again')

    def help(self, value):
        '''Common output messages used throughout interactive mode'''
        print(value)

    def recipes(self):
        '''With user input, inserts general recipe information into the DB.

        Actual insertion is handled by the insert module
        '''

        count = 0
        print(self.intro)

        # User input of recipe information
        while True:
            if count > 0:
                print('\nPlease re-enter the recipe:\n')
            name = input('Recipe name: ')
            desc = input('Recipe description: ')
            meal = input('Meal type (e.g. Breakfast, Main Course, etc.): ')
            cuis = input('Cuisine (e.g. Pasta, Soup or country of origin): ')
            clas = input('Type of recipe (e.g. Vegetarian, Omnivore): ')
            serv = input('Serving size: ')
            print(
                '\n\tRecipe: {}'
                '\n\tDescription: {}'
                '\n\tMeal: {}'
                '\n\tCuisine: {}'
                '\n\tClass: {}'
                '\n\tServing Size: {}\n'
                .format(name, desc, meal, cuis, clas, serv)
            )
            answer = input(self.correct)
            # Inserts data into DB or loops if data is said to be incorrect
            if answer[0].lower() == 'y':

                df = pd.DataFrame(
                    {'name': [name], 'description': [desc],
                     'meal': [meal], 'cuisine': [cuis],
                     'class': [clas], 'servings': [serv]}
                )
                df.to_sql('Recipes', init.conn,
                          if_exists='append', index=False)
                df_recipe_id = pd.read_sql(
                    'select max(id) from Recipes;', init.conn)
                self.id_string_list = []
                self.id_list = []
                recipe_id = df_recipe_id.loc[0][0]
                id_string = str(recipe_id)
                self.id_string_list.append(id_string)
                self.id_list.append(recipe_id)
                # Prepares recipe name output for final of Insert to inform
                # user
                self.recipe = pd.read_sql(
                    'select name from Recipes where id=?', init.conn,
                    params=self.id_string_list
                )
                self.recipe = self.recipe.loc[0][0]
                break
            elif answer[0].lower() == 'n':
                count = count + 1
            else:
                print(self.try_again)
                break
        # Moves to ingredient insertion
        if answer[0].lower() == 'y':
            self.ingredients()

    def ingredients(self):
        '''With user input, inserts recipe ingredients into the DB.'''

        count = 0

        # User input of ingredients for the past recipe
        while True:
            ingd = {}
            if count > 0:
                print('\nPlease re-enter the ingredients:\n')
            num = int(input('How many ingredients?: '))

            # Will search DB for inputted ingredient name, allowing user to
            # select it if correct. This way there are no repeats in
            # Ingredients table.
            existing = []
            for i in range(num):
                ing_list = []
                print('\nIngredient #{}'.format(i+1))
                ing = input('Ingredient name: ')
                ing_string = '%'+ing+'%'
                ing_list.append(ing_string)

                # Checks if inputted ingredient already exists in the DB.
                # If it does, gives the option to use pre-existing as chosen
                # ingredient.
                search = pd.read_sql('''
                        select id, name from Ingredients where name like ?''',
                                     init.conn, params=ing_list
                                     )
                if search.empty:
                    pass
                else:
                    while True:
                        print('\n'+search.to_string(index=False))
                        check = input(
                            '\nDid you mean any of these? If so, type the  '
                            'number associated with the ingredient you want.'
                            '\nOtherwise, type "n": '
                        )
                        if check == 'n':
                            break
                        else:
                            ls = []
                            ls.append(check)
                            ing = pd.read_sql('''
                select name from Ingredients where id=?
                ''', init.conn, params=ls
                                              )
                            ing = ing.loc[0][0]
                            existing.append(ing)
                            try:
                                if not int(ing):
                                    print(
                                        '\nPlease type "n" or enter the '
                                        'number of the correct ingredient'
                                    )
                            except ValueError:
                                break
                amt = input('Ingredient amount: ')
                det = input('Ingredient details: ')

                # Formats ingredients into dictionary for easy SQL insertion
                if not ingd:
                    ingd['Ingredient'] = [ing]
                    ingd['Amount'] = [amt]
                    ingd['Details'] = [det]
                else:
                    ingd['Ingredient'].append(ing)
                    ingd['Amount'].append(amt)
                    ingd['Details'].append(det)
                df = pd.DataFrame(data=ingd)
                dfs = df.to_string(index=False)
            print(
                '\n\t{}\n'.format(dfs)
            )
            answer = input(self.correct)
            # Inserts final input into DB, both into Ingredients table as well
            # as RecipeIngredients, linking the input with Recipe's recipe_id.
            if answer[0].lower() == 'y':

                new_ing = {'name': []}
                for i in ingd['Ingredient']:
                    if i not in existing:
                        new_ing['name'].append(i)
                dfi = pd.DataFrame(new_ing)
                dfi.to_sql('Ingredients', init.conn, if_exists='append',
                           index=False)
                self.ing_id_list = []
                for i in ingd['Ingredient']:
                    param = []
                    param.append(i)
                    get_id = pd.read_sql('''
                        SELECT id from Ingredients where name=?
                        ''', init.conn, params=param)
                    self.ing_id_list.append(get_id.id[0])
                rng = len(self.ing_id_list)
                dfri = pd.DataFrame(
                    {'recipe_id': self.id_list*rng,
                     'ingredient_id': self.ing_id_list,
                     'amount': ingd['Amount'], 'detail': ingd['Details']}
                )
                dfri.to_sql('RecipeIngredients', init.conn, if_exists='append',
                            index=False)
                self.steps()
                break
            elif answer[0].lower() == 'n':
                count = count + 1
            else:
                print(self.try_again)
                break

    def steps(self):
        '''Insert the recipe steps into DB.'''

        '''With user input, inserts recipe steps into the DB.'''

        count = 0
        num = []

        # User input of recipe steps
        print('Once finished inputting recipe instructions, type "exit"\n')
        while True:
            instructions = input('Recipe step: ')
            num.append(instructions)

            if instructions == "exit":
                num.remove("exit")
                print(
                    '\n{}\n\n'.format('\n'.join(num))
                )
                answer = input(self.correct)

                # If data correct, inserts into RecipeSteps table with
                # reference to recipe_id, otherwise loops.
                if answer[0].lower() == 'y':
                    rng = len(num)
                    steps = []
                    for i in range(1, rng+1):
                        steps.append(i)
                    df = pd.DataFrame(
                        {'recipe_id': self.id_list*rng, 'step_number': steps,
                         'instructions': num}
                    )
                    df.to_sql('RecipeSteps', init.conn, if_exists='append',
                              index=False)
                    print('\nThe "{}" recipe has been inputted!'
                          .format(self.recipe)
                          )
                    break
                elif answer[0].lower() == 'n':
                    num = []
                else:
                    print(self.try_again)
                    break
