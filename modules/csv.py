#!/usr/bin/env python3

# pypantry
# Copyright (C) 2018 Prescott Hidalgo-Monroy

# This file is part of pypantry.
#
# pypantry is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypantry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypantry.  If not, see <http://www.gnu.org/licenses/>.

import os
import re
import pandas as pd

from modules import init
init = init.Init()


def csv():
    '''Loads recipe database from a properly set-up CSV.

    Made to be used with a new database, but can be used to append new data if
    there is no existing data in the DB in the CSV
    '''

    files = ['Recipes.csv', 'Ingredients.csv', 'RecipeIngredients.csv',
             'RecipeSteps.csv']

    csvs = os.listdir(import_path)
    imp = []

    # Creates CSV files for input and import if non-existant
    if not csvs:
        answer = input(
            'No files present. Would you like them created? [y/n]: '
        )
        if answer[0].lower() == 'y':
            for i in files:
                os.mknod(import_path+i, mode=0o644)
                result = re.match(r'\w*', i).group(0)
                imp.append(result)
                df = pd.read_sql_query('select * from {};'.format(imp[-1]),
                                       init.conn
                                       )
                headers = list(df)
                headers = init.config['import']['separator'].join(headers)
                csv = open(init.import_path+i, 'w')
                csv.write(headers)
                csv.close()
        else:
            print('Please create files')

    # Verifies whether files are proper before load
    else:
        if all(i in files for i in csvs):
            for i in csvs:
                result = re.match(r'\w*', i).group(0)
                imp.append(result)

            for table in imp:
                name = pd.read_csv(import_path+table+'.csv',
                                   sep='{}'.format(
                                       config['import']['separator']),
                                   index_col='id')
                try:
                    name.to_sql(table, init.conn, if_exists='append')
                    print("CSVs loaded into PyPantry")
                except sqlite3.IntegrityError:
                    answer = input(
                        'Table "{}" already exits. Replace with CSV? [y/n]: '
                        .format(
                            table)
                    )
                    if answer[0].lower() == 'y':
                        name.to_sql(table, init.conn, if_exists='replace')
                        print("CSVs loaded into PyPantry")
                    elif answer[0].lower() == 'n':
                        pass
                        print('Import aborted')
                    else:
                        print('Please choose a correct value and try again')
        else:
            print(
                'Make sure only the correct files are in the import directory'
            )
