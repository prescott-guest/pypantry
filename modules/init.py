#!/usr/bin/env python3

# pypantry
# Copyright (C) 2018 Prescott Hidalgo-Monroy

# This file is part of pypantry.
#
# pypantry is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypantry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypantry.  If not, see <http://www.gnu.org/licenses/>.

import os
import sqlite3
import configparser


class Init:
    def __init__(self):
        '''Initialization of Sqlite connection and read of config.ini.'''

        self.homedir = os.path.expanduser('~')
        self.config = configparser.ConfigParser()
        self.config_path = '{}/.config/pypantry/config.ini'.format(
            self.homedir)
        self.conf = self.config.read(self.config_path)
        if len(self.conf) != 0:
            self.conn = sqlite3.connect(self.config['database']['path'])
            self.cur = self.conn.cursor()
            self.import_path = self.config['import']['path']
        else:
            self.mkconfig()
            self.conn = sqlite3.connect(self.config['database']['path'])
            self.cur = self.conn.cursor()
            self.mkdb()
            self.import_path = self.config['import']['path']

    def mkdb():
        '''Creation of the database if it does not yet exist.'''

        self.cur.execute('''
                CREATE TABLE IF NOT EXISTS Recipes(
                id INTEGER PRIMARY KEY,
                name TEXT NOT NULL,
                description TEXT NOT NULL,
                meal TEXT NOT NULL,
                cuisine TEXT NOT NULL,
                class TEXT NOT NULL,
                servings TEXT NOT NULL);
                ''')
        self.conn.commit()
        self.cur.execute('''
                CREATE TABLE IF NOT EXISTS Ingredients(
                id INTEGER PRIMARY KEY,
                name TEXT NOT NULL);
                ''')
        self.conn.commit()
        self.cur.execute('''
                CREATE TABLE IF NOT EXISTS RecipeIngredients(
                id INTEGER PRIMARY KEY,
                recipe_id INTEGER NOT NULL,
                ingredient_id INTEGER NOT NULL,
                amount TEXT,
                detail TEXT,
                FOREIGN KEY (recipe_id) REFERENCES Recipes(id),
                FOREIGN KEY (ingredient_id) REFERENCES Ingredients(id));
                ''')
        self.conn.commit()
        self.cur.execute('''
                CREATE TABLE IF NOT EXISTS RecipeSteps(
                id INTEGER PRIMARY KEY,
                recipe_id INTEGER NOT NULL,
                step_number INTEGER NOT NULL,
                instructions TEXT NOT NULL,
                FOREIGN KEY (recipe_id) REFERENCES Recipes(id));
                ''')
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def mkconfig(self):
        '''Creation of the config file if it does not yet exist.'''

        self.config['database'] = {
            'path':
                '{}/.local/share/pypantry/cookbook.db'.format(homedir)
        }
        self.config['import'] = {
            'path':
                '{}/.local/share/pypantry/import/'.format(homedir),
            'separator': ';'
        }
        os.makedirs('{}/.config/pypantry/'.format(self.homedir))
        os.makedirs('{}/.local/share/pypantry/import/'.format(self.homedir))
        with open(self.config_path, 'w') as config_file:
            config.write(config_file)
