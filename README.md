# pypantry
##### Recipe Database Management

A fully-functioning culinary management program, where you can save and manage your recipes in one "cookbook". Search for dishes either through recipe name/cuisine/meal/class, as well as by ingredient.

pypantry is still in very early in development, as well as being specifically tailored to my use case. Once more functionality is included, a GUI front-end will be looked at as well as customization of the database itself.

## Features

- [x] Import existing CSVs into pypantry.
- [x] Interactivity/flexibility in import process.
- [x] CLI recipe input.
- [x] Output given search terms.
- [x] Configuration file.
- [ ] Recipe modifications.
- [ ] PyQt or web front-end.

## Arguments

Option | Usage
-------|------
[-l, --load] | Imports CSVs located under 'data/import/'.
[-i, --insert] | Allows CLI input of recipe data.
[-f, --find] | Search functionality by category keywords.
[-r, --random] | Pick a random recipe from your cookbook.
[-s, --set] | Change a config option.

## Installation

#### Prerequisites
- Python3 (tested with 3.5.3).
- Pip.
- Pipenv (recommended).

#### Instructions
```bash
cd desired_directory/
git clone https://salsa.debian.org/prescott-guest/pypantry
cd pypantry
pipenv --three # Initializes Python3 virtual environment if using pipenv
pipenv install # Installs required Python libraries from Pipfile
# or if not using pipenv
pip install -r ./requirements.txt

./pypantry.py [options]
```
