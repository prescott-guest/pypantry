import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pypantry",
    version="0.7.1",
    scripts=['pypantry.py'],
    author="Prescott Hidalgo-Monroy",
    author_email="prescott@hidalgo-monroy.com",
    description="Recipe management program",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://salsa.debian.org/prescott-guest/pypantry",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Utilities"
    ),
    keywords="cooking recipe management cookbook",
    install_requires=['pandas', 'argparse', 'configparser'],
    python_requires='>=3.5'
)
