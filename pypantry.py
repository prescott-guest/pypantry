#!/usr/bin/env python3

# pypantry
# Copyright (C) 2018 Prescott Hidalgo-Monroy

# This file is part of pypantry.
#
# pypantry is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pypantry is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pypantry.  If not, see <http://www.gnu.org/licenses/>.

import sys
from modules import insert
from modules import output
from modules import config
from modules import csv
from modules import arguments

if __name__ == '__main__':
    ins = insert.Insert()
    out = output.Output()
    args = arguments.args

    if args.load:
        csv.csv()
    elif args.insert:
        ins.recipes()
    elif args.find:
        out.find(args.find)
    elif args.random:
        out.random()
    elif args.set:
        config.setting(args.set)

    if not len(sys.argv) > 1:
        print('will run interactive mode here')
